import React from 'react'
import Login from './components/login'
import Layout from './components/layout'

//router
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch
} from 'react-router-dom'

//antd
import 'antd/dist/antd.css'

export default class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isLogin: undefined, // 檢查是否登入
      token: undefined, // 紀錄token,並傳給下層組件讓他們接取api時可以使用
      account: undefined // 紀錄帳號,並傳給下層組件讓他們接取api時可以使用
    }
  }

  componentDidMount() {
    this.checkTokenExist()
  }

  checkTokenExist = () => {
    // 檢查使用者有沒有紀錄token
    if (this.state.isLogin === undefined) {
      if (window.localStorage.getItem('token')) {
        console.log('yes')
        this.setState({
          isLogin: true,
          token: window.localStorage.getItem('token'),
          account: ''
        })
      } else {
        this.setState({
          isLogin: false
        })
      }
    }
  }

  handleLogin = (token, account) => {
    this.setState({
      isLogin: true,
      token,
      account
    })
  }

  handleLogout = () => {
    this.setState({
      isLogin: false,
      token: undefined,
      account: undefined
    })
  }

  renderLogin = () => <Login onLogin={this.handleLogin} />

  renderLayout = props => (
    <Layout
      {...props}
      token={this.state.token}
      account={this.state.account}
      onLogout={this.handleLogout}
    />
  )

  render() {
    return (
      <Router>
        {this.state.isLogin !== undefined ? (
          this.state.isLogin ? (
            window.location.pathname === '/' ? (
              <Redirect to="/Admin" />
            ) : null
          ) : (
            <Redirect to="/" />
          )
        ) : null}
        {this.state.isLogin !== undefined ? (
          <Switch>
            <Route
              path="/Admin/:type?/:path?"
              exact={true}
              component={this.renderLayout}
            />
            <Route path="/" component={this.renderLogin} />
          </Switch>
        ) : null}
      </Router>
    )
  }
}
