import React from 'react'
import { HashRouter as Router } from 'react-router-dom'
import LayoutPage from './view/Layout'

export default class RouterTemplate extends React.PureComponent {
  render() {
    return (
      <Router>
        <LayoutPage />
      </Router>
    )
  }
}
