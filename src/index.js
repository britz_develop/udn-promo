import React from 'react'
import ReactDOM from 'react-dom'
// import App from './App';
// import './sass/reset.sass';
//polyfill
import 'react-app-polyfill/ie9'
import 'react-app-polyfill/stable'
import * as serviceWorker from './serviceWorker'

import RouterTemplate from './Router'
import 'antd/dist/antd.css'
import './styles/index.sass'
import './styles/login.sass'
import './styles/product.sass'

ReactDOM.render(<RouterTemplate />, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
