import React from 'react'
import { Switch, Route, Link } from 'react-router-dom'
import { createHashHistory } from 'history'
import { ConfigProvider, Layout, Button, Menu, Icon } from 'antd'
import Login from './login/Login'
import ProductList from './product/List'
import ProductDetail from './product/Detail'
import logo from '../img/logo.png'
import zhTW from 'antd/es/locale/zh_TW'

export default class LayoutPage extends React.Component {
  constructor(props) {
    super(props)
    this.history = createHashHistory()
    this.state = {
      isInit: false,
      isLogin: false
    }
  }

  componentDidMount() {
    if (sessionStorage.getItem('token') !== null) {
      // should check token is valid
      this.setState({ isInit: true, isLogin: true })
    } else {
      this.history.replace('/')
      this.setState({ isInit: true })
    }
  }

  onLogin = () => {
    this.setState({ isLogin: true })
  }

  onLogout = () => {
    sessionStorage.removeItem('token')
    this.setState({ isLogin: false })
  }

  render() {
    return (
      this.state.isInit &&
      (this.state.isLogin ? (
        <ConfigProvider locale={zhTW}>
          <Layout>
            <Layout.Header className='layout-header'>
              <div className='layout-header-logo'>
                <img src={logo} alt='udn promo' />
              </div>
              <Button onClick={this.onLogout} className='layout-header-logout'>
                <Icon type='logout' />
              </Button>
            </Layout.Header>
            <Layout className='layout-body'>
              <Layout.Sider theme='light' width={240} className='layout-body-sider'>
                <Menu defaultOpenKeys={['Product']} defaultSelectedKeys={['Product']}>
                  <Menu.Item key='Product'>
                    <Link to='/Product'>
                      <Icon type='bars' />
                      商品
                    </Link>
                  </Menu.Item>
                </Menu>
              </Layout.Sider>
              <Layout.Content className='layout-body-content'>
                <Switch>
                  <Route path='/Product/:productId/:mode?' component={ProductDetail} />
                  <Route path='/' component={ProductList} />
                </Switch>
              </Layout.Content>
            </Layout>
          </Layout>
        </ConfigProvider>
      ) : (
        <Login onLogin={this.onLogin} />
      ))
    )
  }
}
