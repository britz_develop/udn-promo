import React from 'react'
import { Input, Button, notification } from 'antd'
import logo from '../../img/logo.png'
import accountData from '../../database/account.json'

export default class Login extends React.Component {
  constructor(props) {
    super(props)
    this.accountRef = null
    this.passwordRef = null
    this.loginData = {
      username: '',
      password: ''
    }
    this.state = {
      usernameInvalid: null,
      passwordInvalid: null
    }
  }

  onChange = (key, event) => {
    this.loginData[key] = event.target.value
    this.setState({ usernameInvalid: null, passwordInvalid: null })
  }

  onLogin = () => {
    this.accountRef.blur()
    this.passwordRef.blur()
    // check field is valid
    let usernameInvalid = null
    let passwordInvalid = null
    if (this.loginData.password.length < 6 || this.loginData.password.length > 20) {
      passwordInvalid = '密碼長度不符規定'
      this.passwordRef.focus()
    } else if (!/^[a-zA-Z0-9]+$/g.test(this.loginData.password)) {
      passwordInvalid = '密碼必須是英文或數字'
      this.passwordRef.focus()
    }
    if (this.loginData.username.length < 5) {
      usernameInvalid = '帳號長度不足'
      this.accountRef.focus()
    } else if (!/^[a-zA-Z0-9]+$/g.test(this.loginData.username)) {
      usernameInvalid = '帳號必須是英文或數字'
      this.accountRef.focus()
    } else if (!/[a-zA-Z]/.test(this.loginData.username[0])) {
      usernameInvalid = '帳號首字必須是英文'
      this.accountRef.focus()
    }
    this.setState({ usernameInvalid, passwordInvalid }, () => {
      if (!usernameInvalid && !passwordInvalid) {
        // check account is valid
        const account = accountData.find(
          (account) =>
            account.username === this.loginData.username &&
            account.password === this.loginData.password
        )
        if (account) {
          notification.success({
            message: '登入成功',
            description: ''
          })
          sessionStorage.setItem('token', 'login-success')
          this.props.onLogin()
        } else {
          notification.error({
            message: '登入失敗',
            description: '帳號或密碼輸入錯誤'
          })
        }
      }
    })
  }

  render() {
    return (
      <div className='login-wrap'>
        <div className='login-board'>
          <div className='login-board-header'>
            <img src={logo} alt='udn promo' />
          </div>
          <div className='login-board-body'>
            <div className='login-field'>
              <Input
                placeholder='請輸入5位以上英數，首字英文'
                onChange={this.onChange.bind(this, 'username')}
                onPressEnter={() => this.passwordRef.focus()}
                ref={(target) => (this.accountRef = target)}
              />
              <div className='login-error-tip'>{this.state.usernameInvalid}</div>
            </div>
            <div className='login-field'>
              <Input.Password
                placeholder='請輸入6-20位英數'
                onChange={this.onChange.bind(this, 'password')}
                onPressEnter={this.onLogin}
                ref={(target) => (this.passwordRef = target)}
              />
              <div className='login-error-tip'>{this.state.passwordInvalid}</div>
            </div>
          </div>
          <div className='login-board-footer'>
            <Button onClick={this.onLogin} type='primary'>
              登入
            </Button>
          </div>
        </div>
      </div>
    )
  }
}
