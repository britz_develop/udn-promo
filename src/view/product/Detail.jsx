import React from 'react'
import { createHashHistory } from 'history'
import { Breadcrumb, Button, Input, InputNumber, message } from 'antd'
import productData from '../../database/product.json'

export default class ProductDetail extends React.Component {
  constructor(props) {
    super(props)
    this.history = createHashHistory()
    this.originalData = {}
    this.state = {
      mode: props.match.params.mode === 'Edit' ? 'Edit' : 'View',
      data: {},
      isInit: false,
      saveLoading: false
    }
  }

  componentDidMount() {
    this.getProductData()
  }

  componentDidUpdate(prevProps) {
    if (prevProps.match.params.productId !== this.props.match.params.productId) {
      this.setState(
        {
          data: {},
          isInit: false
        },
        () => this.getProductData()
      )
    }
  }

  getProductData = () => {
    if (this.props.match.params.productId) {
      // 照理說要找id,但因為測試資料的商品id有重複,故採用index
      this.originalData = JSON.parse(
        JSON.stringify(productData.data[this.props.match.params.productId])
      )
      this.setState({ data: productData.data[this.props.match.params.productId], isInit: true })
    } else {
      this.history.push('/Product')
    }
  }

  goBack = () => {
    this.history.push('/Product')
  }

  changeMode = (mode) => {
    this.history.replace(`/Product/${this.props.match.params.productId}/${mode}`)
    this.setState({ mode, data: JSON.parse(JSON.stringify(this.originalData)) })
  }

  onInputChange = (key, event) => {
    const { data } = this.state
    data[key] = event.target.value
    this.setState({ data })
  }

  onNumberChange = (key, value) => {
    const { data } = this.state
    data[key] = value
    this.setState({ data })
  }

  onSave = () => {
    const { data } = this.state
    if (data.name === '' || data.brand === '' || data.price === null) {
      message.error('請確認必填欄位！')
    } else {
      this.setState({ saveLoading: true }, () => {
        setTimeout(() => {
          this.originalData = JSON.parse(JSON.stringify(this.state.data))
          this.history.replace(`/Product/${this.props.match.params.productId}/View`)
          this.setState({ mode: 'View', saveLoading: false })
        }, 1000)
      })
    }
  }

  render() {
    return (
      this.state.isInit && (
        <>
          <Breadcrumb>
            <Breadcrumb.Item>首頁</Breadcrumb.Item>
            <Breadcrumb.Item href='/#/Product'>商品</Breadcrumb.Item>
            <Breadcrumb.Item href={`/#/Product/${this.props.match.params.productId}`}>
              {this.state.data.name}
            </Breadcrumb.Item>
          </Breadcrumb>
          <div className='product-detail-option'>
            {this.state.mode === 'View' ? (
              <>
                <Button onClick={this.goBack}>返回列表</Button>
                <Button type='primary' onClick={this.changeMode.bind(this, 'Edit')}>
                  編輯商品
                </Button>
              </>
            ) : (
              <>
                <Button onClick={this.changeMode.bind(this, 'View')}>取消編輯</Button>
                <Button type='primary' onClick={this.onSave} loading={this.state.saveLoading}>
                  儲存設定
                </Button>
              </>
            )}
          </div>
          <div className='product-detail-form'>
            <div className='product-detail-group'>
              <p className='product-detail-group-title'>商品編號</p>
              <div className='product-detail-group-content'>
                <div className='display'>{this.state.data.id}</div>
              </div>
            </div>
            <div className='product-detail-group'>
              <p className='product-detail-group-title'>商品名稱 *</p>
              <div className='product-detail-group-content'>
                {this.state.mode === 'View' ? (
                  <div className='display'>{this.state.data.name}</div>
                ) : (
                  <Input
                    value={this.state.data.name}
                    onChange={this.onInputChange.bind(this, 'name')}
                  />
                )}
              </div>
            </div>
            <div className='product-detail-group'>
              <p className='product-detail-group-title'>品牌名稱 *</p>
              <div className='product-detail-group-content'>
                {this.state.mode === 'View' ? (
                  <div className='display'>{this.state.data.brand}</div>
                ) : (
                  <Input
                    value={this.state.data.brand}
                    onChange={this.onInputChange.bind(this, 'brand')}
                  />
                )}
              </div>
            </div>
            <div className='product-detail-group'>
              <p className='product-detail-group-title'>價格 *</p>
              <div className='product-detail-group-content'>
                {this.state.mode === 'View' ? (
                  <div className='display'>
                    $ {this.state.data.price.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                  </div>
                ) : (
                  <InputNumber
                    value={this.state.data.price}
                    min={0}
                    step={1}
                    onChange={this.onNumberChange.bind(this, 'price')}
                  />
                )}
              </div>
            </div>
            <div className='product-detail-group'>
              <p className='product-detail-group-title'>顏色</p>
              <div className='product-detail-group-content'>
                {this.state.mode === 'View' ? (
                  <div className='display'>{this.state.data.color}</div>
                ) : (
                  <Input
                    value={this.state.data.color}
                    onChange={this.onInputChange.bind(this, 'color')}
                  />
                )}
              </div>
            </div>
            <div className='product-detail-group'>
              <p className='product-detail-group-title'>尺寸</p>
              <div className='product-detail-group-content'>
                {this.state.mode === 'View' ? (
                  <div className='display'>{this.state.data.size}</div>
                ) : (
                  <Input
                    value={this.state.data.size}
                    onChange={this.onInputChange.bind(this, 'size')}
                  />
                )}
              </div>
            </div>
          </div>
        </>
      )
    )
  }
}
