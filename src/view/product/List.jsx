import React from 'react'
import { Link } from 'react-router-dom'
import { Breadcrumb, Input, Select, Table, Icon, message } from 'antd'
import productData from '../../database/product.json'

export default class ProductList extends React.Component {
  constructor(props) {
    super(props)
    this.wholeData = productData.data.map((product, index) => {
      return {
        ...product,
        index
      }
    })
    this.searchOption = {
      column: 'id',
      key: ''
    }
    this.state = {
      data: [],
      pagination: {
        current: 1,
        total: productData.data.length,
        pageSize: 20,
        position: 'top',
        size: 'small',
        showSizeChanger: true,
        showQuickJumper: true,
        pageSizeOptions: ['20', '40', '100']
      },
      isInit: false
    }
  }

  componentDidMount() {
    this.setState({
      data: JSON.parse(JSON.stringify(this.wholeData)),
      isInit: true
    })
  }

  getColumns = () => [
    {
      dataIndex: 'brand',
      title: '品牌名稱',
      width: 140
    },
    {
      dataIndex: 'name',
      title: '商品名稱',
      sorter: (a, b) => (a.name < b.name ? -1 : 1)
    },
    {
      dataIndex: 'id',
      title: '商品編號',
      width: 200,
      sorter: (a, b) => (a.id < b.id ? -1 : 1)
    },
    {
      dataIndex: 'price',
      title: '價格',
      width: 120,
      sorter: (a, b) => (a.price < b.price ? -1 : 1),
      render(text) {
        return `$ ${text}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
      }
    },
    {
      dataIndex: 'color',
      title: '顏色',
      width: 160
    },
    {
      dataIndex: 'size',
      title: '尺寸',
      width: 160
    },
    {
      dataIndex: 'index',
      title: '',
      width: 124,
      align: 'right',
      render(text) {
        return (
          <div className='product-list-table-option'>
            <Link to={`/Product/${text}/View`}>
              <Icon type='eye' />
            </Link>
            <Link to={`/Product/${text}/Edit`}>
              <Icon type='edit' />
            </Link>
          </div>
        )
      }
    }
  ]

  onTableChange = (newPagination) => {
    const { pagination } = this.state
    pagination.current = newPagination.current
    pagination.pageSize = newPagination.pageSize
    this.setState({ pagination })
  }

  onSearch = (value) => {
    this.searchOption.key = value
    if (this.searchOption.key === '') {
      message.success('搜尋成功')
      this.setState({ data: JSON.parse(JSON.stringify(this.wholeData)) })
    } else {
      if (this.searchOption.column === 'id' && !/^[a-zA-Z0-9]+$/g.test(this.searchOption.key)) {
        // check key valid
        message.error('商品編號只能以英數內容搜尋')
        return false
      }
      message.success('搜尋成功')
      this.setState({
        data: this.wholeData.filter(
          (product) =>
            (this.searchOption.column === 'id' && product.id.includes(this.searchOption.key)) ||
            (this.searchOption.column === 'name' && product.name.includes(this.searchOption.key))
        )
      })
    }
  }

  render() {
    return (
      this.state.isInit && (
        <>
          <Breadcrumb>
            <Breadcrumb.Item>首頁</Breadcrumb.Item>
            <Breadcrumb.Item href='/#/Product'>商品</Breadcrumb.Item>
          </Breadcrumb>
          <div className='product-list-search'>
            <Select defaultValue='id' onChange={(value) => (this.searchOption.column = value)}>
              <Select.Option value='id'>商品編號</Select.Option>
              <Select.Option value='name'>商品名稱</Select.Option>
            </Select>
            <Input.Search
              enterButton='搜尋'
              allowClear={true}
              onChange={(event) => (this.searchOption.key = event.target.value)}
              onSearch={this.onSearch}
            />
          </div>
          <div className='product-list-table'>
            <Table
              rowKey='index'
              columns={this.getColumns()}
              dataSource={this.state.data}
              pagination={this.state.pagination}
              onChange={this.onTableChange}
            />
          </div>
        </>
      )
    )
  }
}
